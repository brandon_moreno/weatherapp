import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OpenWeatherResponse } from '../interfaces/OpenWeatherResponse.interface';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  // TODO https://home.openweathermap.org/
  private APIKEY = 'f87c0e2aa7b8830f790ffb0dc1b2be14';
  private urlApi = 'http://api.openweathermap.org/data/2.5/weather?';

  constructor( private http: HttpClient, private messageService: MessageService ) { }

  getWeather( city: string, code: string ): Observable<any> {
    this.messageService.add(`Ciudad ${ city }, Codigo: ${ code }`);
    return this.http.get(`${ this.urlApi }q=${ city },${ code }&appid=${ this.APIKEY }`);
  }

}