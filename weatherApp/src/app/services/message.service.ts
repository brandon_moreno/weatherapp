import { Injectable, OnInit } from '@angular/core';
import { ListCity } from '../interfaces/ListCity.interface';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  listCities: ListCity[] = [];

  constructor() {
  }

  add( message ): void {
    this.listCities.push( message );
    localStorage.setItem('cities', JSON.stringify( this.listCities ) );

  }

  loadResults() {

    if ( localStorage.getItem('cities') ) {
      this.listCities = JSON.parse( localStorage.getItem('cities') );
    } else {
      this.listCities = [];
    }
    return this.listCities;
  }

  deleteAll() {

    localStorage.removeItem('cities');
    return [];

  }

}
