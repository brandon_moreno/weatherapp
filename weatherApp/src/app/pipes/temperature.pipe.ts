import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temperature'
})
export class TemperaturePipe implements PipeTransform {

  transform( temp: number ): number {

    const temperature = Math.round( temp - 273.15 );

    return temperature;
  }

}
