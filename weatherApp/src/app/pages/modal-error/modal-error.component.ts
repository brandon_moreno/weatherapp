import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.css']
})
export class ModalErrorComponent implements OnInit {

  weather: Observable<any>;

  spinner: boolean = true;

  constructor( @Inject(MAT_DIALOG_DATA) private data: any ) { }

  ngOnInit(): void {
    this.spinner = true;

    setTimeout(() => {

      this.spinner = false;

      this.weather = this.data;

    }, 1000);
  }

}
