import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { OpenWeatherResponse } from '../../interfaces/OpenWeatherResponse.interface';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  weather: OpenWeatherResponse;

  spinner: boolean = true;

  constructor( @Inject(MAT_DIALOG_DATA) private data: any ) {

    this.spinner = true;

    setTimeout(() => {

      this.spinner = false;

      this.weather = this.data;

    }, 1000);

  }

  ngOnInit(): void {
  }

}
