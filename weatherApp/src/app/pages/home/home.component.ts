import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { OpenWeatherResponse } from '../../interfaces/OpenWeatherResponse.interface';
import { ModalErrorComponent } from '../modal-error/modal-error.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  form: FormGroup;
  weatherData: OpenWeatherResponse;

  constructor( private weatherService: WeatherService, private fb: FormBuilder, public dialog: MatDialog ) {
    this.createForm();
    this.cargarForm();
  }

  ngOnInit(): void {

  }

  openDialog( data ) {
    const dialogRef = this.dialog.open( ModalComponent, {
      width: '100%',
      data: data ,
    });
  }

  openDialogError( data ) {
    const dialogRef = this.dialog.open( ModalErrorComponent, {
      width: '100%',
      data: data,
    });
  }

  createForm(): void {
    this.form = this.fb.group({
      city: ['', [ Validators.required ] ],
      country: ['', [ Validators.required ] ]
    });
  }

  cargarForm() {
    this.form.reset({
      city: '',
      country: ''
    });
  }

  getWeather() {

    if ( this.form.invalid ) { return; }

    const city =  this.form.controls.city.value;
    const code =  this.form.controls.country.value;

    this.weatherService.getWeather( city, code )
      .subscribe( resp => {
          this.openDialog( resp );
          console.log( resp );
      }, err => {
        console.log(err);
        this.openDialogError( err );
      });


  }

}
