import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { ListCity } from '../../interfaces/ListCity.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  messages: ListCity[];

  constructor( private messageService: MessageService ) { }

  ngOnInit(): void {
    this.messages = this.messageService.loadResults();
    console.log( this.messages );
  }

  deleteAll() {
    this.messages = this.messageService.deleteAll();
  }

}
